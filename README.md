KPhotoAlbum
===========

For information about KPhotoAlbum, see the home page at
https://www.kphotoalbum.org/

You may also want to check out the phabricator page at
https://phabricator.kde.org/project/profile/255/

## Continuous Integration Results

KPhotoAlbum actively supports Linux and FreeBSD platforms. You can always check the latest build status on the [pipelines](https://invent.kde.org/graphics/kphotoalbum/-/pipelines) page.
If you can help bring KPhotoAlbum to other platforms as well, we would be happy to hear from you!

[![Build Status](https://invent.kde.org/graphics/kphotoalbum/badges/master/pipeline.svg)](https://invent.kde.org/graphics/kphotoalbum/-/pipelines)<br/>
[![Language grade: C/C++](https://img.shields.io/lgtm/grade/cpp/g/KDE/kphotoalbum.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/KDE/kphotoalbum/context:cpp)<br/>
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/g/KDE/kphotoalbum.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/KDE/kphotoalbum/context:javascript)



Mailing List
============

KPhotoAlbum has a mailing list at

    https://mail.kde.org/cgi-bin/mailman/listinfo/kphotoalbum

where you might find answers to common questions, plus get information
about KPhotoAlbum development, plus of course ask questions.


WIKI
====

KPhotoAlbum has a user wiki at

    https://userbase.kde.org/KPhotoAlbum

For developer topics, there also is a developer-centric wiki at

    https://community.kde.org/KPhotoAlbum


Installation Instructions
=========================

For installation instructions see the INSTALL file.  For a list of
major changes, see the ChangeLog file. A full list of changes is in GIT
log available also on-line:
https://commits.kde.org/kphotoalbum
